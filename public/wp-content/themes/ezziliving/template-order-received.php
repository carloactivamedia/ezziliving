<?php /* Template Name: Order Received */ ?>
<?php get_header(); ?>

<?php 

try {

	global $wpdb;
	
	// $session_id = $_COOKIE['rental_session_order'];
	$get_session_id = $_GET['order'];

	if(empty($get_session_id)) {
		throw new Exception();
	}

	$get_row = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."orders WHERE session_key = '$get_session_id'" );


	if(!$get_row) {
		throw new Exception();
	}

	
} catch (Exception $e) {
	wp_redirect(get_bloginfo('url'));
}


?>

<div id="content-wrapper">

	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title() ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="page-content order-received-page">
		<div class="post-wrapper">
				
			<div class="container">

				<div class="default-page-content">
					<?php
						while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					?>
				</div>

			</div>

		</div>
	</div>

</div>

<?php get_footer(); ?>