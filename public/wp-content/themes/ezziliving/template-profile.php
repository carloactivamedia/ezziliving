<?php /* Template Name: Profile */ ?>

<?php get_header(); ?>

<div id="content-wrapper">

	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title() ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="profile-section section">

		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2">
					<div class="page-content">
						<?php
							while ( have_posts() ) : the_post();
								the_content();
							endwhile;
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="blue-block">
			<div class="container">
				<?php echo wpautop(get_post_meta(get_the_ID(), 'p_blue_box', true)) ?>
			</div>
		</div>
		
		<?php $awards = get_post_meta(get_the_ID(), 'p_awards', true); ?>
		<?php if($awards) { ?>
			<div class="awards-wrapper">
				<div class="container">
					<div class="col-lg-10 offset-lg-1">
						<div class="section-title">
							Awards & Certifications
						</div>
						<?php foreach ($awards as $key => $award) { ?>
							<a href="<?php echo $award ?>" data-fancybox="fancy-awards">
								<img src="<?php echo $award ?>" class="img-fluid awards-image">
							</a>
						<?php } ?>
					</div>
				</div>
			</div>
		<?php } ?>

	</div> <!-- section -->

</div> <!-- content-wrapper -->

<?php get_footer(); ?>