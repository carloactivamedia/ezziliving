<?php get_header(); ?>

<div id="content-wrapper">
	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title() ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="page-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">

					<div class="sidebar-wrapper">
						<div class="sidebar contact-us">
							<div class="widget-title">CONTACT US</div>
							<div class="widget-content">
								<div class="textwidget">
									<p>RPCL Pte Ltd @ Rev Power Car Leasing 10 Kaki Bukit Road 2 #03-15 First East Centre Singapore 417868</p>
									<div class="sidebar-widget agent">
										<div class="dashed-bordered-bottom">
											<div>(Ah Wee)</div>
											<div>6251 0150 / 9653 0015</div>
										</div>
										<div class="dashed-bordered-bottom">
											<div>(Norris)</div>
											<div>9723 0015</div>
										</div>
										<div class="dashed-bordered-bottom">
											<div>(Edmund)</div>
											<div>9011 0015</div>
										</div>
										<div class="dashed-bordered-bottom">
											<div>Fax</div>
											<div>6251 0150</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="col-lg-9">
					<div class="content">
						<?php
							while ( have_posts() ) : the_post();
								the_content();
							endwhile;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<?php get_footer(); ?>