<?php

/*****************************************************************
//																//
// 						AJAX REQUEST       						//
//																//
*****************************************************************/
add_action('wp_ajax_search_form', 'search_form');
add_action('wp_ajax_nopriv_search_form', 'search_form');

function search_form() {
	$post_data = $_POST;

	try {
		$unique_id = esc_attr( uniqid( 'search-form-' ) );
		$search_query = ($post_data['s'] != 'null') ? $post_data['s'] : '';

		$form = '<form class="form-inline my-2 my-lg-0" action="'.esc_url( home_url( '/' ) ).'" role="search" method="get" >
					<div class="input-group">
						<input name="s" type="search" id="'.$unique_id.'" class="form-control" aria-label="Search..." placeholder="Search..." value="'.$search_query.'">
						<div class="input-group-append">
							<span class="input-group-text"><i class="fas fa-search"></i></span>
						</div>
					</div>
				</form>';

		$result['html'] = $form;

	} catch (Exception $e) {
		$result['status'] = false;

		if($e->getMessage() != '') {
			$result['message'] = $e->getMessage();
		}
	}

	wp_send_json($result);
	wp_die();
}