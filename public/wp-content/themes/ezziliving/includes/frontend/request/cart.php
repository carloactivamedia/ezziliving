<?php

add_action('wp_ajax_updateCartAction', 'updateCartAction');
add_action('wp_ajax_nopriv_updateCartAction', 'updateCartAction');
function updateCartAction() {
	$post_data = $_POST;
	$order_id = @$_COOKIE['cart_session_order'];

	try {

		global $wpdb;

		if($post_data == '') {
			throw new Exception('empty post_data');
		}
		
		if(!$order_id) {
			throw new Exception('empty order_id');
		}
		
		if($post_data['quantity'] == 0) {
			// $delete_session = $wpdb->delete( $wpdb->prefix . 'session_order', array( 'session_key' => $order_id ), array( '%s' ) );
			throw new Exception();
		}
		
		$get_row = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."session_order WHERE session_key = '".$order_id."'" );

		if(!$get_row) {
			throw new Exception();
		}

		if($post_data['submit_action'] == 'update') {
			// pass product cart data to a variable;
			$unserialize = unserialize($get_row->session_value);

			// remove duplicate product ID
			foreach ($unserialize['product_cart'] as $key => $product_cart) {
				if($post_data['product'] == $product_cart['product_id']) {
					unset($unserialize['product_cart'][$key]);
				}
			}

			// add new product to the cart
			$unserialize['product_cart'][] = array('product_id' => $post_data['product'],
													'quantity' => $post_data['quantity']);
													
			$form_data = serialize($unserialize);

			$update_session_order = $wpdb->update( 
							$wpdb->prefix . 'session_order', 
							array( 
								'session_value' => $form_data,	// string
							), 
							array( 'session_key' => $order_id ), 
							array( 
								'%s',
							), 
							array( '%s' ) 
						);

			if($update_session_order === false) {
				throw new Exception();
			} 


			$result['success'] = true;
			// $result['url'] = get_bloginfo('url').'/order-received/?order='.$order_id;

		} elseif($post_data['submit_action'] == 'delete') {
			// delete session order
			// Your Request Quote has been successfully sent to the store owner!
			// Your Enquiry Cart is empty! redirecting to the product page shortly.

			$unserialize = unserialize($get_row->session_value);

			// remove duplicate product ID
			foreach ($unserialize['product_cart'] as $key => $product_cart) {
				if($post_data['product'] == $product_cart['product_id']) {
					unset($unserialize['product_cart'][$key]);
				}
			}

			// show_log($unserialize['product_cart']);
			// show_log(count($unserialize['product_cart']));

			if(empty($unserialize['product_cart'])) {
				// show_log('empty');
				$delete_session = $wpdb->delete( $wpdb->prefix . 'session_order', array( 'session_key' => $order_id ), array( '%s' ) );
			} else {
				// show_log('on going cart');
				$form_data = serialize($unserialize);

				$update_session_order = $wpdb->update( 
								$wpdb->prefix . 'session_order', 
								array( 
									'session_value' => $form_data,	// string
								), 
								array( 'session_key' => $order_id ), 
								array( 
									'%s',
								), 
								array( '%s' ) 
							);

				if($update_session_order === false) {
					throw new Exception();
				} 
			}

			$result['success'] = true;
			$result['action'] = 'remove-row';
			$result['url'] = get_permalink(116);
		}

		

	} catch (Exception $e) {
		$result['success'] = false;

		if($e->getMessage() != '') {
			$result['message'] = $e->getMessage();
		}
	}

	wp_send_json($result);
	wp_die();
}


add_action('wp_ajax_checkoutCart', 'checkoutCart');
add_action('wp_ajax_nopriv_checkoutCart', 'checkoutCart');
function checkoutCart() 
{
	$post_data = $_POST;

	$result['post_data'] = $post_data;
	$session_id = @$_COOKIE['cart_session_order'];

	try {
		global $wpdb;
		
		if($post_data == '') {
			throw new Exception('empty post_data');
		}
		
		if($session_id == '') {
			throw new Exception('empty cookie');
		}
		
		$get_row = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."session_order WHERE session_key = '".$session_id."'" );

		if(!$get_row) {
			throw new Exception();
		}

		$cart_detail = unserialize($get_row->session_value);

		$product_detail = get_post($cart_detail['product_id']);

		$wpdb->insert( $wpdb->prefix . 'orders', 
				array(
					'order' 				=> $get_row->session_value,
					'session_key' 			=> $session_id,
					'first_name' 			=> $post_data['first_name'],
					'last_name' 			=> $post_data['last_name'],
					'email' 				=> $post_data['email'],
					'phone' 				=> $post_data['phone'],
					'company_name' 			=> $post_data['company_name'],
					'street_address' 		=> $post_data['street_address'],
				), 
				array( 
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s'
				) 
			);

		if(!$wpdb->insert_id) {
			throw new Exception('dd');
		} else {
			$order_id = $wpdb->insert_id;
		}

		$delete_session = $wpdb->delete( $wpdb->prefix . 'session_order', array( 'session_key' => $session_id ), array( '%s' ) );

		if (isset($_COOKIE['cart_session_order'])) {
			unset($_COOKIE['cart_session_order']);
			setcookie('cart_session_order', null, -1, '/');
		}

		$result['success'] = true;
		$result['url'] = get_bloginfo('url').'/order-received/?order='.$session_id;

		/************
		* New Order
		* Email Notification to [Client]
		*****************/
		$subject 	= 'New Quote Request ( Request: #'.$order_id.' ) - '.date('F j, Y');
		$from 		= 'Ezzi Living <webform@website.ezziliving.com>';
		
		$mail_message 	= 'You have received quote request from '.$post_data['first_name'].' '.$post_data['last_name'].'. The detail is(are) as follows:';
		$header_text 	= 'Request Quote';
		$email_template = generage_email_template($order_id, $header_text, $mail_message);
		$result['email_template'] = $email_template;

		/* loop if multiple recipients has been set*/
		$email_setting = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."order_settings WHERE name = 'recipients'");
		if($email_setting) {
			$get_recipient = $email_setting->value;
			if($get_recipient != '') {
				$get_recipient = explode(',', $get_recipient);

				if(is_array($get_recipient)) {
					foreach ($get_recipient as $key => $recipient) {
						if (filter_var(trim($recipient), FILTER_VALIDATE_EMAIL)) {
							$reply_to = $post_data['first_name'].' '.$post_data['last_name'].' <'.$post_data['email'].'>';
							$send_email = do_send_email(trim($recipient), null, null, $from, $subject, $email_template, $reply_to);
						}
					}
				}
			}
		}

		/************
		* New Order
		* Email Notification to [Customer]
		*****************/
		$subject 	= 'Your Request Quote from '.date('F j, Y');
		$from 		= 'Ezzi Living <webform@website.ezziliving.com>';
		$recipient 	= $post_data['email'];
		
		$mail_message 	= 'Your request has been received and is now being processed. Your request details are shown below for your reference:';
		$header_text 	= 'Thank you';
		$email_template = generage_email_template($order_id, $header_text, $mail_message);

		$send_email 	= do_send_email($recipient, null, null, $from, $subject, $email_template);

		// if($send_email) {
		// 	$result['email'] = 'sent';
		// } else {
		// 	$result['email'] = 'failed';
		// }
		
	} catch (Exception $e) {
		$result['success'] = false;

		if($e->getMessage() != '') {
			$result['message'] = $e->getMessage();
		}
	}

	wp_send_json($result);
	wp_die();	
}


add_action('wp_ajax_cart_counter', 'cart_counter');
add_action('wp_ajax_nopriv_cart_counter', 'cart_counter');

function cart_counter() {
	$session_id = @$_COOKIE['cart_session_order'];

	try {
		global $wpdb;
		
		if($session_id == '') {
			throw new Exception('empty cookie');
		}

		$get_row = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."session_order WHERE session_key = '".$session_id."'" );

		if(!$get_row) {
			throw new Exception();
		}

		$cart_detail = unserialize($get_row->session_value);

		$result['counter'] = count($cart_detail['product_cart']);

		$result['status'] = true;

	} catch (Exception $e) {
		$result['status'] = false;

		if($e->getMessage() != '') {
			$result['message'] = $e->getMessage();
		}
	}

	wp_send_json($result);
	wp_die();
}