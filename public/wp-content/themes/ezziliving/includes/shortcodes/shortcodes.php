<?php

/**
 * name: col
 * usage: [col]xxxx[/col]
 */
function col_shortcode($atts, $content = null) {
	extract(
		shortcode_atts(
			array(
				'class' => 'col-lg-6'
			),
			$atts
		)
	);
	return '<div class="'.$class.'">'.$content.'</div>';
}
add_shortcode('col', 'col_shortcode');

/**
 * name: colContainer
 * usage: [colContainer]xxxx[/colContainer]
 */
function colContainer_shortcode($atts, $content = null) {
	return '<div class="container">
				<div class="row">
					'.do_shortcode($content).'
				</div>
			</div>';
}
add_shortcode('colContainer', 'colContainer_shortcode');

function url_shortcode() {
	return get_bloginfo('url');
}
add_shortcode('url','url_shortcode');

function shortcode_empty_paragraph_fix( $content ) {

    // define your shortcodes to filter, '' filters all shortcodes
    $shortcodes = array( 'blueBlock' );

    foreach ( $shortcodes as $shortcode ) {

        $array = array (
            '<p>[' . $shortcode => '[' .$shortcode,
            '<p>[/' . $shortcode => '[/' .$shortcode,
            $shortcode . ']</p>' => $shortcode . ']',
            $shortcode . ']<br />' => $shortcode . ']'
        );

        $content = strtr( $content, $array );
    }

    return $content;
}
add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );

/**
 * name: pageTitle
 * usage: [pageTitle]xxxx[/pageTitle]
 */
function pageTitle_shortcode($atts, $content = null) {
	extract(
			shortcode_atts(
				array(
					'heading_tag' => 'div'
				),
				$atts
			)
	);
	return '<'.$heading_tag.' class="section-title">'.$content.'</'.$heading_tag.'>';
}
add_shortcode('pageTitle', 'pageTitle_shortcode');

/**
 * name: blueBlock
 * usage: [blueBlock]xxxx[/blueBlock]
 */
function blueBlock_shortcode($atts, $content = null) {
	return '<div class="blue-block">'.$content.'</div>';
}
add_shortcode('blueBlock', 'blueBlock_shortcode');


/**
 * name: Video Tag
 * usage: [video]xxxx[/video]
 */
function video_shortcode($atts, $content = null) {
	// return '<div class="blue-block">'.$content.'</div>';
	extract(
			shortcode_atts(
				array(
					'width' => '',
					'height' => '',
					'mp4' => '',
					'poster' => ''
				),
				$atts
			)
	);
	return '<div class="embed-responsive embed-responsive-16by9">
				<video class="embed-responsive-item" controls="controls" poster="'.$poster.'" src="'.$mp4.'" type="video/mp4"></video>
			</div>';
}
add_shortcode('video', 'video_shortcode');