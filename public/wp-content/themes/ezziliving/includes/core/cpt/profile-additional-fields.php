<?php

function cmb2_profile_additional_fields() {
	$prefix = 'p_';

	$cmb = new_cmb2_box( array(
		'id'            => 'additional_fields',
		'title'         => __( 'Additional Fields', 'cmb2' ),
		'object_types'  => array( 'page' ), // Post type
		'show_on'      => array( 'key' => 'id', 'value' => array( 9 ) ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Highlighted Content', 'cmb2' ),
		'id'   => $prefix . 'blue_box',
		'type' => 'wysiwyg',
		'options' => array(
			'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
		)
	) );

	$cmb->add_field( array(
		'id'   => $prefix . 'awards',
		'name' => esc_html__( 'Awards & Certifications', 'cmb2' ),
		'type' => 'file_list',
		'preview_size' => array( 80, 80 ), // Default: array( 50, 50 )
	) );
}
add_action( 'cmb2_admin_init', 'cmb2_profile_additional_fields' );