<?php

function cmb2_product_specs() {
	$prefix = 'p_';

	/**
	 * Product Details
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'product_specs',
		'title'         => __( 'Product Detail', 'cmb2' ),
		'object_types'  => array( 'product' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Product Availability', 'cmb2' ),
		'id'   				=> $prefix . 'availability',
		'type'             => 'select',
		'show_option_none' => true,
		'default'          => 'In Stock',
		'options'          => array(
			'In Stock' => __( 'In Stock', 'cmb2' ),
			'Out of Stock'   => __( 'Out of Stock', 'cmb2' ),
		),
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Gallery', 'cmb2' ),
		'id'   => $prefix . 'product_gallery',
		'type' => 'file_list',
		'preview_size' => array( 62, 62 ), // Default: array( 50, 50 )
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Description & Specification', 'cmb2' ),
		'id'   => $prefix . 'description_specification',
		'type' => 'wysiwyg',
		'options' => array(
			'textarea_rows' => 10, // rows="..."
		)
	) );


	$cmb->add_field(array(
		'id' => $prefix . 'product_demonstration',
		'name' => __('Product Demonstration', 'cmb2'),
		'desc' => 'Embedded code of video',
		'type' => 'wysiwyg',
		'options' => array(
			'textarea_rows' => 10, // rows="..."
		)
	));


	/**
	 * Products portfolio
	 */
	$cmb_portfolio = new_cmb2_box( array(
		'id'            => 'product_portfolio',
		'title'         => __( 'Product Portfolio', 'cmb2' ),
		'object_types'  => array( 'product' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true
	) );

	$cmb_portfolio->add_field( array(
		'name' => esc_html__( 'Portfolio', 'cmb2' ),
		'id'   => $prefix . 'product_portfolio',
		'type' => 'file_list',
		'preview_size' => array( 62, 62 ), // Default: array( 50, 50 )
	) );


	/**
	 * FAQ Meta box
	 */
	$cmb_faq = new_cmb2_box( array(
		'id'            => 'product_faq',
		'title'         => __( 'Product FAQ', 'cmb2' ),
		'object_types'  => array( 'product' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true
	) );

	$cmb_faq->add_field( array(
		'name' => esc_html__( 'FAQ', 'cmb2' ),
		'id'   => $prefix . 'product_faq',
		'type' => 'wysiwyg',
		'options' => array(
			'textarea_rows' => 10, // rows="..."
		)
	) );
}
add_action( 'cmb2_admin_init', 'cmb2_product_specs' );