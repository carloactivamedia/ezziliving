<?php

function cmb2_testimonial() {
	$prefix = 'p_';

	$cmb1 = new_cmb2_box( array(
		'id'            => 'testimonial-detail',
		'title'         => __( 'Testimonials Detail', 'cmb2' ),
		'object_types'  => array( 'testimonial' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$cmb1->add_field( array(
		'name' => esc_html__( 'Location', 'cmb2' ),
		'id'   => $prefix . 'location',
		'type' => 'text'
	) );

	$cmb1->add_field( array(
		'name'             => 'Stars',
		'id'               => $prefix . 'stars',
		'type'             => 'select',
		'show_option_none' => true,
		'default'          => '5',
		'options'          => array(
			'5' => __( '5 Stars', 'cmb2' ),
			'4' => __( '4 Stars', 'cmb2' ),
			'3' => __( '3 Stars', 'cmb2' ),
			'2' => __( '2 Stars', 'cmb2' ),
			'1' => __( '1 Star', 'cmb2' )
		),
	) );

	$cmb = new_cmb2_box( array(
		'id'            => 'testimonial-video',
		'title'         => __( 'Video Detail', 'cmb2' ),
		'object_types'  => array( 'testimonial' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Video Link', 'cmb2' ),
		'id'   => $prefix . 'video-link',
		'type' => 'file'
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Video Thumbail', 'cmb2' ),
		'id'   => $prefix . 'video-thumbail',
		'type' => 'file'
	) );
	
	
}
add_action( 'cmb2_admin_init', 'cmb2_testimonial' );