<?php /* Template Name: Products */ ?>

<?php get_header(); ?>

<?php $view_mode = (@$_GET['view'] == 'list') ? 'list' : 'grid'; ?>

<div id="content-wrapper">

	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title() ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="products-section section">

		<div class="container">
			<div class="row">
				
				<div class="col-12">
					<?php if( get_post()->post_content !== '' ) { ?>
						<div class="page-content">
							<?php
								while ( have_posts() ) : the_post();
									the_content();
								endwhile;
							?>
						</div>
					<?php } ?>

					<div class="product-wrapper">

						<div class="toolbar">
							<div class="row">
								<div class="col-lg-5 col-md-5">
									<div class="list-style">
										<ul class="list-inline">
											<li class="list-inline-item">
												<a href="<?php echo get_permalink(116) ?>?view=grid" class="<?php echo ($view_mode == 'grid') ? 'active' : '' ?>">
													<i class="fas fa-th"></i>
												</a>
											</li>
											<li class="list-inline-item">
												<a href="<?php echo get_permalink(116) ?>?view=list" class="<?php echo ($view_mode == 'list') ? 'active' : '' ?>">
													<i class="fas fa-align-justify"></i>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-lg-7 col-md-7">
									<div class="sort-selection">
										Sort By:
										<div class="btn-group">
										<button type="button" class="btn btn-link btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<?php
												switch (@$_GET['sort']) {
													case 'newest':
														echo 'Newest';
														break;
													
													case 'a-z':
														echo 'A-Z';
														break;
													
													case 'z-a':
														echo 'Z-A';
														break;
													
													default:
														echo 'Newest';
														break;
												}
											?>
										</button>
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" href="<?php echo appendQueryString( array('sort' => 'newest') ) ?>">Newest</a>
											<a class="dropdown-item" href="<?php echo appendQueryString( array('sort' => 'a-z') ) ?>">Name (A-Z)</a>
											<a class="dropdown-item" href="<?php echo appendQueryString( array('sort' => 'z-a') ) ?>">Name (Z-A)</a>
										</div>
										</div>
									</div>
									<div class="display-counter">
										<label>
											<?php
												$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
												$product_counter_qry = array(
													'post_type' 	=> 'product',
													'post_status'	=> 'publish',
													'orderby' 		=> 'post_date',
													'order'			=> 'DESC',
													'posts_per_page' => -1
												);
												$product_counter = new wp_query( $product_counter_qry );
												$posts_per_page = get_option('posts_per_page');

												$post_count = $product_counter->post_count;

												if($paged == 1) {
													$showing_first = 1;
													$last = $posts_per_page*$paged;
												} else {
													$showing_first = ($posts_per_page*($paged-1))+1;
													$last = $posts_per_page*$paged;
												}

												if($last > $post_count) {
													$last = $post_count;
												}
											?>
											Showing <?php echo $showing_first."-".$last; ?> of <?php echo $post_count ?> results
										</label>
									</div>
								</div>
							</div>
						</div> <!-- toolbar -->

						<div class="product-<?php echo $view_mode ?>">
							<?php
								$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
								
								$product_args = array(
									'post_type' 	=> 'product',
									'post_status'	=> 'publish',
									'paged' 		=> $paged
								);

								switch (@$_GET['sort']) {
									case 'a-z':
										$product_args = array_merge( $product_args, array('orderby' => 'title', 'order' => 'ASC') );
									break;

									case 'z-a':
										$product_args = array_merge( $product_args, array('orderby' => 'title', 'order' => 'DESC') );
									break;

									case 'newest':
										$product_args = array_merge( $product_args, array('orderby' => 'post_date', 'order' => 'DESC') );
										break;
									
									default:
										$product_args = array_merge( $product_args, array('orderby' => 'post_date', 'order' => 'DESC') );
										break;
								}

								$products = new wp_query( $product_args );
							?>	
							<?php if($products->have_posts()) { ?>
								<div class="row">
									<?php while ( $products->have_posts() ) { ?>
									<?php 
											$products->the_post();
											$product_thumbnail_id 	= get_post_thumbnail_id( $products->post->ID );

											$product_thumbnail 		= wp_get_attachment_image_src($product_thumbnail_id, 'medium');
											$product_thumbnail_lg 	= wp_get_attachment_image_src($product_thumbnail_id, 'large');

											$product_thumbnail 		= ($product_thumbnail[0] != '') ? $product_thumbnail[0] : get_bloginfo('template_url').'/dist/images/no-image.jpg';
											$product_thumbnail_lg 	= ($product_thumbnail_lg[0] != '') ? $product_thumbnail_lg[0] : get_bloginfo('template_url').'/dist/images/no-image.jpg';
									?>
									<?php if($view_mode == 'grid') { ?>
											<div class="col-lg-3 col-md-4 col-sm-6">
												<a href="<?php echo get_permalink($products->post->ID) ?>">
													<div class="product-item">
														<div class="product-thumb" style="background-image: url('<?php echo @$product_thumbnail_lg ?>')">
														</div>
														<div class="product-name">
															<?php echo $products->post->post_title ?>
														</div>
													</div>
												</a>
											</div>
										<?php } else { ?>
											<div class="col-lg-12">
												<div class="product-item">
													<!-- <a href="<?php echo get_permalink($products->post->ID) ?>"> -->
														<a href="<?php echo get_permalink($products->post->ID) ?>">
															<div class="product-thumb" style="background-image: url('<?php echo @$product_thumbnail_lg ?>')"></div>
														</a>
														<div class="product-detail">
															<div class="product-name">
																<a href="<?php echo get_permalink($products->post->ID) ?>">
																	<?php echo $products->post->post_title ?>
																</a>
															</div>
															<div class="product-description">
																<?php echo substr(strip_tags(get_post_meta(get_the_ID(), 'p_product_description', true)), 0, 150) ?>
																<?php echo $products->post->post_content ?>
															</div>
															<div class="list-button">
																<a href="<?php echo get_permalink($products->post->ID) ?>" class="btn btn-site">
																	View Product
																</a>
															</div>
														</div>
													<!-- </a> -->
												</div>
											</div>
										<?php } // view_mode ?>
									<?php } ?>
								</div>
							<?php } else { ?>

								<div class="no-product">
									<div class="text-center">
										<h4>No products to show</h4>
									</div>
								</div>
							<?php } ?>
							
						</div> <!-- product-grid -->

						<?php //base_pagination($products->max_num_pages) ?>
						<?php cs_pagination($products->max_num_pages) ?>

					</div> <!-- product-grid-wrapper -->
				</div>
				
			</div>
		</div>

	</div> <!-- section -->

</div> <!-- content-wrapper -->

<?php get_footer(); ?>