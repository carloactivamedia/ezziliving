<?php get_header(); ?>

<div id="content-wrapper">

	<div class="banner-wrapper">
		<div class="container">
			<div class="banner">
				<div class="banner-text-wrapper">
					<img src="<?php echo get_bloginfo('template_url') ?>/dist/images/banner-bg.jpg" alt="" class="img-fluid">
					<div class="banner-text">
						<div class="first-text">
							The
						</div>
						<div class="second-text">
							Clotheslines <br>Specialist
						</div>
						<div class="third-text">
							More than a decade<br> of providing solutions to<br> hanging laundry!
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- About Section -->
	<div class="about-section section">
		<div class="container">
			<div class="row about-section-row">
				<div class="col-lg-6">
					<img src="<?php echo get_bloginfo('template_url') ?>/dist/images/about-us-img-2.jpg" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6">
					<div class="section-title">
						Welcome to
						<span>EZ Living</span>
					</div>
					
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/_TkDn2LDp_A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			<div class="section-content">
				<?php
					while ( have_posts() ) : the_post();
						the_content();
					endwhile;
				?>
				<a href="<?php echo get_permalink(9) ?>" class="btn btn-site">Read More</a>
				<a href="<?php echo get_permalink(13) ?>" class="btn btn-site white">Enquire Now</a>
			</div>
		</div>
	</div>

	<div class="d-none d-sm-none d-lg-block">
		<!-- Specs Section -->
		<?php get_template_part('template-parts/specs') ?>
	</div>

	<!-- Automatic Section -->
	<?php get_template_part('template-parts/automatic') ?>
	<!-- Manual Section -->
	<?php get_template_part('template-parts/manual') ?>
	<!-- Ironing Section -->
	<?php get_template_part('template-parts/ironing') ?>

	<div class="d-lg-none mobile-specs-wrapper">
		<!-- Specs Section -->
		<?php get_template_part('template-parts/specs') ?>
	</div>

	<!-- Testimonials Section -->
	<?php get_template_part('template-parts/testimonials') ?>
	
</div>

<?php get_footer() ?>