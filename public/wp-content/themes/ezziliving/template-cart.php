<?php /* Template Name: Cart */ ?>
<?php get_header(); ?>
<?php 

try {

	global $wpdb;
	
	$session_id = @$_COOKIE['cart_session_order'];
	
	// dd($session_id);
	
	// $table_name = $wpdb->prefix.'session_order';
	// if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
	// 	dd('wala');
	// }
	// else{
	// 	dd('meron');
	// }


	$get_session_id = @$_GET['order'];
	$empty_cart = false;

	if(empty($session_id)) {
		throw new Exception();
	}
	
	// if($session_id != $get_session_id) {
	// 	throw new Exception();
	// }

	$get_row = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."session_order WHERE session_key = '$session_id'" );


	if(!$get_row) {
		throw new Exception();
	}

	$cart = unserialize($get_row->session_value);

	
} catch (Exception $e) {
	$empty_cart = true;
	// wp_redirect(get_bloginfo('url'));
	// Your Request Quote has been successfully sent to the store owner!
}


?>

<div id="content-wrapper">
	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title() ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="page-content checkout-page section">
		<div class="post-wrapper">

			<div class="container">
				<?php if($empty_cart) { ?>
					
					<div class="default-page-content">
						<div class="text-center">
							<h3>
								Your Enquiry Cart is empty.
							</h3>
						</div>
					</div>

				<?php } else { ?>
					<div class="form-notification" id="form-notification">
						
					</div>
					
					<form id="cart-form">

						<h3 class="block-title">
							YOUR REQUEST
						</h3>

						<div class="table-responsive-sm">
							<div class="table-responsive-xs table-responsive-sm">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th class="product-photo">Image</th>
											<th class="product-name">Product Name</th>
											<th class="product-quantity">Quantity</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach ($cart['product_cart'] as $key => $product) {
												$prod_detail = get_post($product['product_id']);
											?>
												<tr class="cart-row row-<?php echo $product['product_id'] ?>">
													<td>
														<div class="product-photo">
															<?php
															$post_thumbnail_id 	= get_post_thumbnail_id( $product['product_id'] );

															$thumbnail_thumb 	= wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
															$thumbnail_thumb 	= ($thumbnail_thumb[0] != '') ? $thumbnail_thumb[0] : '';
															?>
															<a href="<?php echo get_permalink($prod_detail->ID) ?>"><img src="<?php echo $thumbnail_thumb ?>" alt="" width="50"></a>
														</div>
													</td>
													<td>
														<div class="product-name">
															<a href="<?php echo get_permalink($prod_detail->ID) ?>"><?php echo $prod_detail->post_title ?></a>
														</div>
													</td>
													<td class="product-quantity">
														<ul class="list-inline">
															<li class="list-inline-item">
																<input type="number" class="form-control" name="quantity" pattern="[0-9]*" value="<?php echo $product['quantity'] ?>">
															</li>
															<li class="list-inline-item">
																<button type="button" class="btn btn-link update-cart" data-action="update" data-product="<?php echo $product['product_id'] ?>"><i class="fas fa-edit"></i></button>
															</li>
															<li class="list-inline-item">
																<button type="button" class="btn btn-link update-cart" data-action="delete" data-product="<?php echo $product['product_id'] ?>"><i class="fas fa-trash-alt"></i></button>
															</li>
														</ul>
													</td>
												</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					
						<div class="row">
							<div class="col-lg-6">
								<h3 class="block-title">
									CUSTOMER DETAILS
								</h3>

								<div class="block-content">
									<div class="billing-content">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="label">First Name *</label>
													<input class="form-control alt" type="text" name="first_name" data-validation="required" placeholder="First Name">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="label">Last Name *</label>
													<input class="form-control alt" type="text" name="last_name" data-validation="required" placeholder="Last Name">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="label">Email Address *</label>
											<input class="form-control alt" type="email" name="email" placeholder="Email Address" data-validation="email">
										</div>
										<div class="form-group">
											<label class="label">Phone *</label>
											<input class="form-control alt" type="text" name="phone" data-validation="required" placeholder="Phone">
										</div>
										<div class="form-group">
											<label class="label" for="street_address">Street address *</label>
											<textarea class="form-control" placeholder="Street address" data-validation="required" name="street_address" id="street_address" cols="30" rows="4"></textarea>
										</div>
										<div class="form-group">
											<label class="label">Company Name</label>
											<input class="form-control alt" type="text" name="company_name" placeholder="Company Name" value="">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="submit-wrapper clearfix">
							<button type="submit" class="btn btn-site float-right" id="place_order">Request Quote</button>
						</div>

					</form>
				<?php } ?>
			</div>


		</div>
	</div>

</div>

<?php get_footer(); ?>