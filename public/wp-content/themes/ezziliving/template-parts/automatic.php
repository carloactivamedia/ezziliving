<div class="sidebyside-section automatic section">
    <div class="sidebyside-column">
        <div class="column detail-column">
                <div class="category-name">
                    Automatic
                </div>
                <div class="category-subname">
                    Laundry System
                </div>
                <div class="description">
                    No effort required to lift up heavy laundry loads. Perfect for elderly use. Convenient and hassle-free.
                </div>
                <div class="video-wrapper">
                    <div class="video">
                        <a href="<?php echo get_bloginfo('template_url') ?>/dist/videos/automatic-laundry-system.mp4" data-fancybox="automatic" class="fancy-video">
                            <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/automatic-laundry-system-video-thumbnail.jpg" alt="" class="img-tag img-fluid img-thumbnail">
                        </a>
                        <div class="embed-responsive embed-responsive-16by9">
                            <video class="embed-responsive-item" controls="controls" poster="<?php echo get_bloginfo('template_url') ?>/dist/images/automatic-laundry-system-video-thumbnail.jpg" src="<?php echo get_bloginfo('template_url') ?>/dist/videos/automatic-laundry-system.mp4" type="video/mp4"></video>
                        </div>
                    </div>
                </div>
                <a href="<?php echo get_term_link(22) ?>" class="btn btn-site">View All Automatic Products</a>
            </div>
        <div class="column slider-column">
            <div class="sidebyside-carousel owl-carousel owl-theme">
                <div class="item">
                    <div class="product-image-bg" style="background-image: url('<?php echo get_bloginfo('template_url') ?>/dist/images/dummy-automatic-laundry-system.jpg')"></div>
                </div>
            </div>
        </div>
    </div>
</div>