<div class="sidebyside-section manual section">
    <div class="sidebyside-column">
        <div class="column detail-column">
                <div class="category-name">
                    Manual
                </div>
                <div class="category-subname">
                    Laundry System
                </div>
                <div class="description">
                    Our Manual Systems are designed to help you in organizing and drying your laundry load. Choose from our extensive Eco-friendly indoor and outdoor range.
                </div>
                <div class="video-wrapper">
                    <div class="video">
                        <a href="<?php echo get_bloginfo('template_url') ?>/dist/videos/manual-laundry-system.mp4" data-fancybox="manual" class="fancy-video">
                            <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/manual-laundry-system-video-thumbnail.jpg" alt="" class="img-tag img-fluid img-thumbnail">
                        </a>
                        <div class="embed-responsive embed-responsive-16by9">
                            <video class="embed-responsive-item" controls="controls" poster="<?php echo get_bloginfo('template_url') ?>/dist/images/manual-laundry-system-video-thumbnail.jpg" src="<?php echo get_bloginfo('template_url') ?>/dist/videos/manual-laundry-system.mp4" type="video/mp4"></video>
                        </div>
                    </div>
                </div>
                <a href="<?php echo get_term_link(16) ?>" class="btn btn-site">View All Manual Products</a>
            </div>
        <div class="column slider-column">
            <div class="sidebyside-carousel owl-carousel owl-theme">
                <div class="item">
                    <div class="product-image-bg" style="background-image:url('<?php echo get_bloginfo('template_url') ?>/dist/images/dummy-manual-laundry-system.jpg');"></div>
                </div>
            </div>
        </div>
    </div>
</div>