<div class="wrap">
	<h1>Request Quote</h1>
	<br/>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<table id="table-orders" class="dt-table table table-bordred table-striped">
					<thead>
						<th>Request</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th></th>
						<th>Date</th>
						<th>Actions</th>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="view-order" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Request Quote Information</h4>
			</div>
			<div class="modal-body">
				<div id="modal-loading" class="modal-loading text-center">
					Loading Information
				</div>
				<div class="model-notification"></div>
				<div class="modal-body-content">
					
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<h3 class="order-number"></h3>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
						</div>
					</div>

					<div class="table-responsive-sm">
						<table class="table table-bordered table-condensed">
							<thead>
								<tr>
									<th class="product-name">Product Information</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="order-item">

									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<h3 class="block-title">
						Customer Details
					</h3>

					<table class="table table-bordered">
						<tr>
							<th>Name:</th>
							<td>
								<span class="name"></span>
							</td>
						</tr>
						<tr>
							<th>Email:</th>
							<td>
								<span class="email"></span>
							</td>
						</tr>
						<tr>
							<th>Phone:</th>
							<td>
								<span class="phone"></span>
							</td>
						</tr>
						<tr>
							<th>Address:</th>
							<td>
								<span class="address"></span>
							</td>
						</tr>
						<tr>
							<th>Company Name:</th>
							<td>
								<span class="company_name"></span>
							</td>
						</tr>
					</table>
					<br>
				</div>
			</div>
			<div class="modal-footer clearfix">
				<div class="row">
					<div class="col-md-6">
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="confirmation-order" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<div class="model-notification"></div>
				<div class="modal-notification-mesasge">
					<p>Are you sure you want to accept Order <strong>#<span class="order-number"></span></strong>?</p>
				</div>
			</div>
			<div class="modal-footer clearfix">
				<button type="button" class="btn btn-success" data-order-id="" data-confirm-type="" id="action-order"></button>
				<button type="button" class="btn btn-default" data-order-id="" data-toggle="modal" data-target="#view-order" data-dismiss="modal" id="close-modal">Close</button>
			</div>
		</div>
	</div>
</div>