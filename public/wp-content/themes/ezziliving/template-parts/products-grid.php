<div class="our-product-section section">
    <div class="container">
        <div class="section-title">
            Our Products
        </div>
        <div class="section-content">
            <?php 
                $property_cat = get_terms([
                    'taxonomy' 		=> 'product_category',
                    'hide_empty' 	=> false
                ]);

                $category_order = array(7,6,11,9);
            ?>

            <?php if(!empty($property_cat)) { ?>
                <div class="tab-wrapper">
                    <div class="tab-nav">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <?php foreach ($property_cat as $key => $term) { ?>
                                <?php if(in_array($term->term_id, array(6,7,9,11))) { ?>
                                    <li data-order="<?php echo array_search($term->term_id, $category_order) ?>" class="nav-item <?php echo ($term->term_id == 7) ? 'active' : ''?>"><a class="nav-link <?php echo ($term->term_id == 7) ? 'active show' : ''?>" id="<?php echo $term->slug ?>-tab" data-toggle="tab" role="tab" aria-controls="<?php echo $term->slug ?>" aria-selected="true" href="#<?php echo $term->slug ?>"><?php echo $term->name ?></a></li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                    
                    <div class="tab-content" id="myTabContent">
                        <?php foreach ($property_cat as $key2 => $term2) { ?>
                            <?php if(in_array($term2->term_id, array(6,7,9,11))) { ?>
                                <div id="<?php echo $term2->slug ?>" role="tabpanel" aria-labelledby="<?php echo $term2->slug ?>-tab" class="tab-pane <?php echo ($term2->term_id == 7) ? 'active' : ''?>">

                                    <div class="product-grid">

                                        <?php
                                            $product_args = array(
                                                'post_type' 	=> 'product',
                                                'post_status'	=> 'publish',
                                                'post_date' 	=> 'date',
                                                'order'			=> 'DESC',
                                                'posts_per_page' => 8,
                                                'tax_query' 	=> array(
                                                                        array(
                                                                            'taxonomy' => $term2->taxonomy,
                                                                            'field' => 'id',
                                                                            'terms' => $term2->term_id
                                                                        )
                                                                    )
                                            );
                                            $products = new wp_query( $product_args );
                                            $products_counter = 0;
                                        ?>	
                                        <?php if($products->have_posts()) { ?>
                                            <div class="row">
                                                <?php while ( $products->have_posts() ) { $products_counter++;?>
                                                <?php 
                                                    $products->the_post();
                                                    $product_thumbnail_id 	= get_post_thumbnail_id( $products->post->ID );

                                                    $product_thumbnail 		= wp_get_attachment_image_src($product_thumbnail_id, 'medium');
                                                    $product_thumbnail_lg 	= wp_get_attachment_image_src($product_thumbnail_id, 'large');

                                                    $product_thumbnail 		= ($product_thumbnail[0] != '') ? $product_thumbnail[0] : '';
                                                    $product_thumbnail_lg 	= ($product_thumbnail_lg[0] != '') ? $product_thumbnail_lg[0] : '';
                                                ?>
                                                        <div class="col-lg-3 col-md-6">
                                                            <a href="<?php echo get_permalink($products->post->ID) ?>">
                                                                <div class="product-item">
                                                                    <div class="product-thumb" style="background-image: url('<?php echo @$product_thumbnail_lg ?>')">
                                                                    </div>
                                                                    <div class="product-name">
                                                                        <?php echo $products->post->post_title ?>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                <?php } ?>
                                            </div>
                                            <?php if($products_counter >= 8) { ?>
                                                <div class="text-center">
                                                    <a href="<?php echo get_term_link($term2->term_id) ?>" class="btn btn-site">View All</a>
                                                </div>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <div class="text-center">
                                            <h3>No products to show</h3>
                                            </div>
                                        <?php } ?>

                                    </div> <!-- product-grid -->

                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            
            
        </div>
    </div>
</div>