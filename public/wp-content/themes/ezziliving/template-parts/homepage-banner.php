<div class="homepage-banner-wrapper">
    <div class="homepage-banner">
        <div class="container">
            <div class="banner-content-wrapper">

                <div class="row">
                    <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-8 offset-2">
                        <div class="content-detail">
                            <div class="primary-text">
                            THE <br>CLOTHLINES<br> SPECIALIST
                            </div>
                            <div class="secondary-text">
                            More than a decade of providing solutions to hanging laundry!
                            </div>
                            <a href="http://primepropertiesph.com/itownships/uptown-bonifacio-township/" class="btn btn-site">VIEW OUR PRODUCTS</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>