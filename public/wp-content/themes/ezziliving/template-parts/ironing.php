<div class="ironing-section section">
    <div class="container">
        <div class="inner-section">

            <div class="row">
            
                <div class="col-lg-5 d-flex align-items-center">
                    <div class="product-category-wrapper">
                        <div class="product-category">
                            Ironing
                        </div>
                        <p>Convenient and practical! Saves floor space. Choose between mobile and wall mount ironing boards.</p>
                        <a href="<?php echo get_term_link(21) ?>" class="btn btn-site">View All Ironing Products</a>
                    </div>
                </div>
                <div class="col-lg-7">
                    <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/ironing-image.png" alt="" class="img-fluid">
                </div>

            </div>

        </div> <!-- inner-section -->
    </div>
</div> <!-- ironing-section -->