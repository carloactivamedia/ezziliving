<?php /* Template Name: Projects */ ?>

<?php get_header(); ?>

<div id="content-wrapper">

	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title() ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="page-section section">



		<div class="container">
			<h3 class="page-title">Completed Condominium projects</h3>

			<div class="dynamic-map-wrapper">
				<div id="map" style="width: 100%; height: 400px;" data-icon="<?php echo get_bloginfo('template_url') ?>/dist/images/">
					Loading Map...
				</div>
			</div>

			<div class="content">
				<?php
					while ( have_posts() ) : the_post();
						the_content();
					endwhile;
				?>
			</div>
		</div>

	</div> <!-- section -->

</div> <!-- content-wrapper -->

<!-- <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script> -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBH8vZQuNP4c6XNMShC6X2_SHlXFFP5RcU&callback=map" type="text/javascript"></script>
<script>
	function map() {
		var locations = [
			['Amaryllis Ville', 1.314808,103.8397703, 'default'],
			['Bayou Residence', 1.3463629,103.8780659, 'default'],
			['Bishan Point', 1.3597501,103.8304337, 'default'],
			['City View', 13.7609204,121.0630144, 'default'],
			['Belle Vue', 13.9725874,120.7290473, 'default'],
			['Gardens @ Bishan', 1.3648162,103.8284585, 'default'],
			['Casa Merah', 1.3291018,103.944, 'default'],
			['Gardenville', 1.3124782,103.8274348, 'default'],
			['Draycott 8', 1.310343,103.8316701, 'default'],
			['Heritage Apartments', 1.2883831,103.8492294, 'default'],
			['Natural Loft', 1.3562678,103.844073, 'default'],
			['The Peak', 1.3366381,103.8446832, 'default'],
			['Kovan Melody', 1.3599044,103.8831536, 'default'],
			['Newton 18', 1.3144162,103.8412283, 'default'],
			['Le Crescendo', 1.3308194,103.8886688, 'default'],
			['Orchard Scotts', 1.309647,103.8391089, 'default'],
			['Leonie Studio', 1.2987119,103.8316934, 'default'],
			['One Amber', 1.3014879,103.8981665, 'default'],
			['Helios', 1.306856,103.837965, 'default'],
			['The Marq', 1.3005228,103.8289081, 'default'],
			['Parc Palais', 1.357555,103.766283, 'default'],
			['Pasedena', 1.3152654,103.8454535, 'default'],
			['Quinterra', 1.3186322,103.7806052, 'default'],
			['Double Bay', 1.341269,103.9562873, 'default'],
			['Regency Suites', 1.2844179,103.8269547, 'default'],
			['River Place', 1.2893974,103.8405991, 'default'],
			['Sanctuary Green', 1.296988,103.8714679, 'default'],
			['The Gale', 1.3588299,103.9671609, 'default'],
			['Southbank', 1.3063075,103.8642902, 'default'],
			['Sunflower Lodge', 1.3138647,103.887716, 'default'],
			['Sin Guan Hin Mansion', 1.3073826,103.8535889, 'default'],
			['Tessarina', 1.3324443,103.7870963, 'default'],
			['Summerhill', 1.3583646,103.7658723, 'default'],
			['The Amston', 1.356726,103.760429, 'default'],
			['Park Central', 1.372174,103.852553, 'default'],
			['The Bale', 1.3150849,103.9063559, 'default'],
			['The Crest @ Cairnhill', 1.305288,103.838422, 'default'],
			['The Esta', 1.3019743,103.900035, 'default'],
			['The Estiva', 1.287907,103.778385, 'default'],
			['The Fernhill', 1.3143163,103.8259991, 'default'],
			['The Florida E.C.', 1.3737935,103.9005751, 'default'],
			['The Ford@Holland', 1.313498,103.793691, 'default'],
			['The Grange', 1.301418,103.8237261, 'default'],
			['The Light@Cairnhill', 1.3063763,103.8384123, 'default'],
			['The Nexus', 1.3344211,103.7868975, 'default'],
			['The Premier@Tampines', 1.3566593,103.9388706, 'default'],
			['The Regency@Tiong Bahru', 1.2821842,103.8301556, 'default'],
			['The Riverine', 1.3097802,103.8690967, 'default'],
			['The Serenade', 1.3185139,103.7801784, 'default'],
			['The Sterling', 1.3356307,103.7817883, 'default'],
			['The Solitaire', 1.315065,103.827273, 'default'],
			['The Waterina', 1.312923,103.8906059, 'default'],
			['Tomlinson Mansion', 1.3045898,103.8238706, 'default'],
			['Twin Regency', 1.2848078,103.8285043, 'default'],
			['Waterplace', 1.2963243,103.8720701, 'default'],
			['Waterford Residences', 1.2938768,103.8393664, 'default'],
			['Vertis', 1.3015669,103.899858, 'default'],
			['Watermark Robertson Quay', 1.2909583,103.8381538, 'default'],
			['Versilia on Haig', 1.3121902,103.8959146, 'default']
		];

        var icons = {
			default: {
				icon: '<?php echo get_bloginfo('template_url') ?>/dist/images/default.png'
			},
			logo: {
				icon: '<?php echo get_bloginfo('template_url') ?>/dist/images/ezziliving-marker.png'
			}
        };
	
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 12,
			center: new google.maps.LatLng(1.335059,103.861146),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
	
		var infowindow = new google.maps.InfoWindow();
	
		var marker, i;
	
		for (i = 0; i < locations.length; i++) {  
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			// icon: icons[locations[i][3]].icon,
			map: map
		});
	
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
			infowindow.setContent(locations[i][0]);
			infowindow.open(map, marker);
			}
		})(marker, i));
		}
	}
</script>

<?php get_footer(); ?>