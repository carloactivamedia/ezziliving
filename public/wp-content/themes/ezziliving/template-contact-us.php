<?php /* Template Name: Contact Us */ ?>

<?php get_header(); ?>

<div id="content-wrapper">

	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title() ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="contact-us-section section">

		<div class="content">
			<?php
				while ( have_posts() ) : the_post();
					the_content();
				endwhile;
			?>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">

					<div class="address-wrapper">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="address-block">
									<div class="d-flex align-items-center">
										<div class="address-icon">
											<i class="fas fa-map-marker-alt"></i>
										</div>
										<div class="address">
											36 Jalan Pemimpin #02-09 Pemimpin Industrial Building, Singapore 577219<br>Tel No: <a href="tel:6259 9928">6259 9928</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="address-block">
									<div class="d-flex align-items-center">
										<div class="address-icon">
											<i class="fas fa-map-marker-alt"></i>
										</div>
										<div class="address">
											Showroom Opening Hours:<br>
											Monday to Friday: 10am to 6pm<br>
											Saturday: 10am to 4pm
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>	
		</div>
		
		<div class="map-wrapper">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1994.354361531091!2d103.8420602581465!3d1.3513063812181074!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da173dbe2e2501%3A0x4b2065e08c4d4df8!2sEz+Living+Pte+Ltd!5e0!3m2!1sen!2sus!4v1536740991046" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>

		<div class="container">
			<div class="row">

				<div class="col-lg-3 offset-lg-1">
					<div class="hotlines-wrapper">
						<div class="section-title">
							Sales Hotlines:
						</div>
						<table class="table">
							<tr>
								<td>Andy</td>
								<td><a href="tel:90094499">90094499</a></td>
							</tr>
							<tr>
								<td>Bernard</td>
								<td><a href="tel:84983592">84983592</a></td>
							</tr>
							<tr>
								<td>Jeff</td>
								<td><a href="tel:81004404">81004404</a></td>
							</tr>
							<tr>
								<td>Pann</td>
								<td><a href="tel:96916580">96916580</a></td>
							</tr>
							<tr>
								<td>Ben</td>
								<td><a href="tel:91444042">91444042</a></td>
							</tr>
							<tr>
								<td>Angeline</td>
								<td><a href="tel:94527000">94527000</a></td>
							</tr>
						</table>
					</div>
				</div>

				<div class="col-lg-7">
					<div class="form-wrapper">
						<div class="section-title">
							Send us an enquiry
						</div>
						
						<!-- feedback.activamedia.com.sg script begins here -->
						<script type="text/javascript" defer src="//feedback.activamedia.com.sg/embed/4194485.js" data-role="form" data-default-width="100%"></script>
						<!-- feedback.activamedia.com.sg script ends here -->
					</div>
				</div>

			</div>
		</div>

	</div> <!-- section -->

</div> <!-- content-wrapper -->

<?php get_footer(); ?>