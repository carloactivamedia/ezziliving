// -------------------------------------
// Custom Admin Scripts For Custom Pages
// -------------------------------------
jQuery(document).ready(function($){
	// Initialize Plugin(s)
	$('[data-toggle="tooltip"]').tooltip();

	// Datatable - Orders
	$('#table-orders').DataTable({
		processing: true,
		serverSide: true,
		dom: 'Bfrtip',
		columnDefs: [
			{
				"targets": [ 4 ],
				"visible": false
			}
		],
		buttons: [
		],
		ajax: {
			url: wpAjax.ajaxUrl,
			type: 'POST',
			data: { action: 'get_orders' },
		},
		columns: [
			{
				data: 'order_id',
				orderable: false,
				render: function(data, type, full, meta){
					return '<span class="order-number" data-order-id="'+data+'">#'+data+'</span>';
				}
			},
			{ data: 'first_name' },
			{ data: 'last_name' },
			{ data: 'email' },
			{ data: 'date_created' },
			{ data: 'date_created', dateFormat: 'mm/dd/yy' },
			{
				data: 'order_id',
				orderable: false,
				render: function(data, type, full, meta){
					return '<div class="btn-group"> \
								<button type="button" class="btn btn-default view-order" data-order-id="'+data+'" data-toggle="modal" data-target="#view-order"> \
									<span class="glyphicon glyphicon-eye-open"></span> \
								</button> \
							</div>';
				}
			}
		],
		order: [[5, 'desc']]
	});

	$(document).on('click', '#update-recipients',function(evt){
		evt.preventDefault();

		var $this = this;

		$.ajax({
			type: 'post',
			url: wpAjax.ajaxUrl,
			data: {
					action: 'update_recipients',
					recipients: $("textarea[name='recipients']").val()
					},
			dataType: 'json',
			beforeSend: function(){
				$($this).html('Loading...').attr('disabled', 'disabled');
				$("#form-notification").html('');
			},
			success: function(res){
				if(res.success == true) {
					$("#form-notification").html('<div class="alert alert-success" role="alert">'+res.message+'</div>');

				} else {
					$("#form-notification").html('<div class="alert alert-danger" role="alert">There an error occurred while processing your request. Please try again later</div>');
				}
			},
			complete: function(){
				$($this).html('Submit').removeAttr('disabled');
			}
		});
	});

	$('#view-order').on('hide.bs.modal', function (event) {
		$('#view-order .modal-body-content').fadeIn();
		$('#view-order .name').html('');
		$('#view-order .email').html('');
		$('#view-order .phone').html('');
		$('#view-order .address').html('');
		$('#view-order .note').html('');
		$('#view-order .order-number').html('');

		$('.modal-body-content').fadeOut();
		$('#view-order .action-buttons').fadeOut();
	});


	$('#view-order').on('shown.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var order_id = button.data('order-id');
		var modal = $(this);
		console.log('showing' + order_id);

		$.ajax({
			type: 'post',
			url: wpAjax.ajaxUrl,
			data: {
					action: 'getRequestQuote',
					order_id: order_id
					},
			dataType: 'json',
			beforeSend: function(){
				$('#modal-loading').show();
				$("#req-accept-order").removeAttr('disabled');
				$("#req-cancel-order").removeAttr('disabled');
				$("#req-process-order").removeAttr('disabled');
			},
			success: function(res){
				if(res.status == 1) {
					$('#view-order .modal-body-content').fadeIn();
					$('#view-order .order-item').html(res.data.product_list_html);
					$('#view-order .name').html(res.data.name);
					$('#view-order .email').html(res.data.email);
					$('#view-order .phone').html(res.data.phone);
					$('#view-order .address').html(res.data.address);
					$('#view-order .company_name').html(res.data.company_name);
					$('#view-order .order-number').html('Request #'+order_id+' details');

					$('#view-order .action-buttons button').each(function(){
						$(this).data('order-id', order_id);
					});
					
					if(res.data.order_status == 1) {
						$("#req-accept-order").attr('disabled', 'disabled');
						$(".order-status-label").removeClass('cancelled process').addClass('accepted').html('Accepted')
					} else if(res.data.order_status == 2) {
						$("#req-cancel-order").attr('disabled', 'disabled');
						$(".order-status-label").removeClass('accepted process').addClass('cancelled').html('Cancelled')
					} else if(res.data.order_status == 0) {
						$("#req-process-order").attr('disabled', 'disabled');
						$(".order-status-label").removeClass('cancelled accepted').addClass('processing').html('Processing')
					}

					$('#view-order .action-buttons').fadeIn();

				} else if (res.status == 0) {
					$('.model-notification').html(res.message);
				} else {
					$('.model-notification').html('');
				}
			},
			complete: function(){
				$('#modal-loading').hide();
			}
		});
	});

});