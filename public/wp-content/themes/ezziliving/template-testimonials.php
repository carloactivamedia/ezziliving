<?php /* Template Name: Testimonials */ ?>

<?php get_header(); ?>

<div id="content-wrapper">

	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title( '', false ) ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="masonry-wrapper section masonry">
			
			<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$testimonial_args = array(
					'post_type' 	=> 'testimonial',
					'post_status'	=> 'publish',
					'orderby' 		=> 'date',
					'order'			=> 'DESC',
					'posts_per_page'=> -1,
					'paged' 		=> $paged
				);
				$testimonial = new wp_query( $testimonial_args );
			?>	
			<?php if($testimonial->have_posts()) { ?>
				<?php while ( $testimonial->have_posts() ) { ?>
				<?php 
					$testimonial->the_post();
					$product_thumbnail_id 	= get_post_thumbnail_id( $testimonial->post->ID );

					$product_thumbnail 		= wp_get_attachment_image_src($product_thumbnail_id, 'medium');
					$product_thumbnail_lg 	= wp_get_attachment_image_src($product_thumbnail_id, 'large');

					$product_thumbnail 		= ($product_thumbnail[0] != '') ? $product_thumbnail[0] : '';
					$product_thumbnail_lg 	= ($product_thumbnail_lg[0] != '') ? $product_thumbnail_lg[0] : '';
					
					$video_link = get_post_meta($testimonial->post->ID, 'p_video-link', true);
					$video_thumb = get_post_meta($testimonial->post->ID, 'p_video-thumbail', true);
				?>		
					<div class="item<?php echo ($video_link != '') ? ' stamp' : '' ?>">
						<div class="testimonial-quote">
							<i class="fas fa-quote-left"></i>
						</div>
						<?php if($video_link != '') { ?>
							<div class="testimonial-video">
								<a href="<?php echo $video_link ?>" data-fancybox="" class="fancy-video d-none d-md-none d-lg-block d-xl-block">
									<img src="<?php echo $video_thumb ?>" alt="" class="img-tag img-fluid img-thumbnail">
								</a>
								<div class="embed-responsive embed-responsive-16by9 d-block d-md-block d-lg-none d-xl-none">
									<video class="embed-responsive-item" controls="controls" poster="<?php echo $video_thumb ?>" src="<?php echo $video_link ?>" type="video/mp4"></video>
								</div>
							</div>
						<?php } ?>
						<div class="testimonial">
							<?php echo nl2br($testimonial->post->post_content) ?>
						</div>
						<div class="stars">
							<?php $stars = get_post_meta($testimonial->post->ID, 'p_stars', true); ?>
							<?php for ($i=0; $i < 5; $i++) { ?>
								<i class="fa<?php echo ($i >= $stars) ? 'r' : 's' ?> fa-star"></i>
							<?php } ?>
						</div>
						<div class="testimonial-author">
							<?php echo $testimonial->post->post_title ?>
						</div>
					</div>
				<?php } ?>
			<?php } ?>

		</div>

	</div>

</div>

<?php get_footer(); ?>