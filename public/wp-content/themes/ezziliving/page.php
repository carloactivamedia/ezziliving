<?php get_header(); ?>

<div id="content-wrapper">

	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title() ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="page-section section">

		<div class="container">
			<div class="content">
				<?php
					while ( have_posts() ) : the_post();
						the_content();
					endwhile;
				?>
			</div>
		</div>

	</div> <!-- section -->

</div> <!-- content-wrapper -->

<?php get_footer(); ?>