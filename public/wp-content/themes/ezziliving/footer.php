<!-- FOOTER -->

<?php if(!is_front_page()) { ?>
	<div class="footer-separator"></div>
	<div class="separator-tail"></div>
<?php } ?>

<div id="footer-wrapper">
	<div class="top-footer">
		<div class="container">
			<img src="<?php echo get_bloginfo('template_url') ?>/dist/images/logo.png" alt="Ezzi Living" class="logo-footer">
			<div class="address">
				63 Jalan Pemimpin #02-09 Pemimpin Industrial Building, Singapore 577219
			</div>
			<div class="opening-hour">
				Showroom Opening Hours: Monday to Friday - 10am to 6pm | Saturday - 10am to 4pm
			</div>
			<div class="call-us">
				Call us at:
			</div>
			<a href="tel:6259 9928" class="tel-link">6259 9928</a>
		</div>
	</div>

	<div class="botton-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-12 order-lg-2">
					<?php 
						if ( has_nav_menu( 'footer_links' ) ) :
							$args = array(
								'menu' 				=> 'footer_links',
								'theme_location'  	=> 'footer_links',
								'container_class'	=> 'footer-links');
							wp_nav_menu($args);
						endif;
					?>
				</div>
				<div class="col-lg-6 col-12 order-lg-1">
					<p>Website maintained by <a href="http://www.activamedia.com.sg/">Activa Media</a>. <span class="reserved">All rights reserved 2018.</span></p>
				</div>
				
			</div>
		</div>
	</div>
</div>

<a id="cart-link" href="<?php echo get_bloginfo('url').'/enquiry-cart/' ?>"><i class="fas fa-shopping-cart"></i></a>

<!-- <div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div> -->

<?php wp_footer(); ?>

<div class="whatsapp-btn">
	<a target="_blank" href="https://web.whatsapp.com/send?phone=6594527000&amp;text=" class="web"><i class="fab fa-whatsapp"></i></a>
	<a target="_blank" href="https://api.whatsapp.com/send?phone=6594527000&amp;text=" class="api"><i class="fab fa-whatsapp"></i></a>
</div>

</body>
</html>
<?php ob_end_flush(); ?>