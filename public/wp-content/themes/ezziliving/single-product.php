<?php get_header(); ?>
<?php 

	$cart_prod_id 	= @$_GET['add-to-cart'];
	$get_prod_id 	= get_the_ID();
	$notif_show 	= false;
	$notif_class 	= 'alert-success';
	$notif_message  = '';

	try {

		global $wpdb;

		// /*****************
		// * get all blocked dates 
		// *******************/
		// $blocked_dates = $wpdb->get_results("SELECT 
		// 										pickup_date,
		// 										return_date
		// 									FROM ".$wpdb->prefix."orders
		// 									WHERE order_status = 1
		// 									AND prod_id = ".$get_prod_id."
		// 									AND pickup_date > now()
		// 									AND return_date > now()
		// 									ORDER BY pickup_date ASC");

		// if($blocked_dates) {
		// 	$blocked_dates_array = array();
		// 	foreach ($blocked_dates as $key => $blocked_date) {
		// 		$days_diff = days_diff($blocked_date->pickup_date, $blocked_date->return_date);
		// 		for ($i=0; $i < ($days_diff+1); $i++) { 
		// 			array_push($blocked_dates_array, "'".date('m/d/Y', strtotime($blocked_date->pickup_date .' +'.$i.' day'))."'");
		// 		}
		// 	}
		// 	$blocked_dates_array = implode('|', $blocked_dates_array);
		// }

		// show_log($_POST);


		// $form_data = $_POST;
		// $form_data['prod_id'][] = $get_prod_id;
		$form_data['product_cart'][] = array('product_id' => $get_prod_id,
											'quantity' => @$_POST['quantity-field']);
		// array_push($form_data['product_cart'], array('product_id' => $get_prod_id,
		// 											'quantity' => $_POST['quantity-field'])
		// 			);
		// $array1 = $form_data['product_cart'];
		// $array2 = array('product_id' => $get_prod_id,
		// 				'quantity' => $_POST['quantity-field']);
		// $form_data = array_merge($array1, $array2);


		if(!is_numeric($cart_prod_id)) {
			throw new Exception();
		} else {
			$cart_prod_id = (int)$cart_prod_id;
		}

		if($cart_prod_id !== $get_prod_id) {
			throw new Exception();
		}

		if(!$_POST) {
			throw new Exception();
		} else {
			if(@$_POST['quantity-field'] <= 0) {
				throw new Exception('Sorry, please enter the quantity before putting it to the cart');
			}
		}

		/**********************
		 * check the availability of the product
		 **********************/
		$is_available = get_post_meta(get_the_ID(), 'p_availability', true); 
		if($is_available == 'Out of Stock') {
			throw new Exception('Sorry, This product is out of stock.');
		}

		
		// /*****************
		// * pickup date and return date verification 
		// *******************/
		// if($form_data['pickup_date'] == '' || $form_data['return_date'] == '') {
		// 	throw new Exception('Please provide Pickup Date and Return Date');
		// } else {
		// 	if(strtotime($form_data['pickup_date']) > strtotime($form_data['return_date'])) {
		// 		throw new Exception('Please select different date');
		// 	}
		// }

		// $pickup_date = date('m/d/Y H:i:s', strtotime($form_data['pickup_date']));
		// $return_date = date('m/d/Y H:i:s', strtotime($form_data['return_date']));

		// $is_date_taken = $wpdb->get_row("SELECT
		// 									*
		// 								FROM 
		// 									".$wpdb->prefix."orders
		// 								WHERE 
		// 									order_status = 1
		// 								AND
		// 									prod_id = ".$get_prod_id."
		// 								AND
		// 								(
		// 									STR_TO_DATE(DATE_FORMAT(pickup_date, '%m/%d/%Y %H:%i:%s'), '%m/%d/%Y %H:%i:%s') <= STR_TO_DATE('".$return_date."', '%m/%d/%Y %H:%i:%s') 
		// 									AND 
		// 									STR_TO_DATE(DATE_FORMAT(return_date, '%m/%d/%Y %H:%i:%s'), '%m/%d/%Y %H:%i:%s') >= STR_TO_DATE('".$pickup_date."', '%m/%d/%Y %H:%i:%s')
		// 								)
		// 								");

		// if($is_date_taken) {
		// 	throw new Exception('Date is already taken. Please select different date');
		// }


		// $additional_charges_array 	= $form_data['additional_charges'];

		// if(!empty($additional_charges_array)) {
		// 	$charges_key = array();
		// 	$add_charges = array();

		// 	foreach ($additional_charges_array as $key => $additional_charges_array_val) {
		// 		array_push($charges_key, $key);
		// 	}

		// 	$get_additional_charges 	= get_post_meta(get_the_ID(), 'p_charges', true);

		// 	foreach ($get_additional_charges as $key_charge => $charge) {
		// 		if(in_array($key_charge, $charges_key)) {
		// 			array_push($add_charges, array($charge['p_item_name'], $charge['p_description'], $charge['p_cost']));
		// 		}
		// 	}

		// 	$form_data['additional_charges'] = $add_charges;
		// }
		
		if (!isset($_COOKIE['cart_session_order'])) {

			$session_id = uniqid();

			session_order_setcookie($session_id);

			$wpdb->insert( $wpdb->prefix . 'session_order', 
					array( 
						'session_key'     => $session_id,
						'session_value' => serialize($form_data)
					), 
					array( 
						'%s',
						'%s'
					) 
				);
			if(!$wpdb->insert_id) {
				throw new Exception();
			} else {
				$notif_show = true;
			}

		} else {

			$session_id = $_COOKIE['cart_session_order'];

			$get_row = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."session_order WHERE session_key = '$session_id'" );

			if($get_row) {
				
				// pass product cart data to a variable;
				$unserialize = unserialize($get_row->session_value);

				// remove duplicate product ID
				foreach ($unserialize['product_cart'] as $key => $product_cart) {
					if($get_prod_id == $product_cart['product_id']) {
						unset($unserialize['product_cart'][$key]);
					}
				}

				// add new product to the cart
				$unserialize['product_cart'][] = array('product_id' => $get_prod_id,
														'quantity' => @$_POST['quantity-field']);
														
				$form_data = $unserialize;

				$update_session_order = $wpdb->update( 
									$wpdb->prefix . 'session_order', 
									array( 
										'session_value' => serialize($form_data),	// string
									), 
									array( 'session_key' => $session_id ), 
									array( 
										'%s',
									), 
									array( '%s' ) 
								);

				if($update_session_order === false) {
					throw new Exception();
				} else {
					$notif_show = true;
				}

			} else {
				
				$wpdb->insert( $wpdb->prefix . 'session_order', 
					array( 
						'session_key'     => $session_id,
						'session_value' => serialize($form_data)
					), 
					array( 
						'%s',
						'%s'
					) 
				);

			}

		}

		$notif_message = sprintf('Success: You have added <strong>%s</strong> to your <a href="%s" class="btn btn-sm btn-secondary">Enquiry Cart!</a>', get_the_title(), get_bloginfo('url').'/enquiry-cart/?order='.$session_id);

		$notif_show = true;
		
	} catch (Exception $e) {
		if($e->getMessage() != '') {
			$notif_show = true;
			$notif_class = 'alert-danger';
			$notif_message = $e->getMessage();
		}
	}

?>

<div id="content-wrapper">

	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo get_the_title() ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="product-page-section section">

		<div class="container">
			<div class="row">

				<div class="col-lg-10 offset-lg-1 col-md-12">

					<div class="post-wrapper">

						<?php if($notif_show) { ?>
							<div class="alert <?php echo $notif_class; ?>" role="alert">
								<?php echo $notif_message ?>
							</div>
						<?php } ?>

						<form action="<?php echo get_permalink(get_the_ID()) ?>?add-to-cart=<?php echo get_the_ID() ?>" method="POST">

							<div class="product-quick-view">
								<div class="block-content">
									<div class="row">
										<div class="col-md-6">
											<?php 
												$post_thumbnail_id 	= get_post_thumbnail_id( get_the_ID() );

												$thumbnail_thumb 	= wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
												$thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');

												$caption 			= wp_get_attachment_caption( $post_thumbnail_id );

												$thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/no-image.jpg';
												$thumbnail_thumb 	= ($thumbnail_thumb[0] != '') ? $thumbnail_thumb[0] : get_bloginfo('template_url').'/dist/images/no-image.jpg';

												$gallery = get_post_meta(get_the_ID(), 'p_product_gallery', true);
												
											?>
											<div class="gallery-wrapper">
												<div class="gallery-carousel owl-carousel owl-theme">
													<?php if($thumbnail != '') { ?>
														<div class="item">
															<div class="gallery-thumbnail img-thumbnail" class="owl-lazy" style="background-image: url('<?php echo $thumbnail ?>')">
																<a href="<?php echo $thumbnail ?>" data-key="<?php echo $post_thumbnail_id ?>" data-fancybox="gallery" data-caption="<?php echo $caption ?>">
																	<div class="go-fancy">
																		<i class="fas fa-arrows-alt-h"></i>
																	</div>
																</a>
															</div>
															<?php if($caption != '') { ?>
																<div class="gallery-caption">
																	<span><?php echo $caption ?></span>
																</div>
															<?php } ?>
														</div>
													<?php } ?>
													
													<?php if($gallery != '') { ?>
														<?php foreach ($gallery as $key => $image) { ?>
															<?php $caption = wp_get_attachment_caption( $key ); ?>
															<div class="item">
																<div class="gallery-thumbnail img-thumbnail" style="background-image: url('<?php echo $image ?>')">
																	<a href="<?php echo $image ?>" data-key="<?php echo $key ?>" data-fancybox="gallery" data-caption="<?php echo $caption ?>">
																		<div class="go-fancy">
																			<i class="fas fa-arrows-alt-h"></i>
																		</div>
																	</a>
																</div>
																<?php if($caption != '') { ?>
																	<div class="gallery-caption">
																		<span><?php echo $caption ?></span>
																	</div>
																<?php } ?>
															</div>
														<?php } ?>
													<?php } ?>
												</div>

												<div class="gallery-thumbnail-wrapper">
													<div id="gallery-thumbnail" class="owl-carousel owl-theme">
														<?php if($thumbnail_thumb != '') { ?>
															<div class="item">
																<a href="#" onclick="jQuery('.gallery-carousel').trigger('to.owl.carousel', [0,300]); return false;">
																	<img class="img-thumbnail" src="<?php echo $thumbnail_thumb ?>">
																</a>
															</div>
														<?php } ?>
														<?php $thumbnail = 0; ?>
														<?php if($gallery != '') { ?>
															<?php foreach ($gallery as $key_2 => $image_2) { $thumbnail++; ?>
																<?php $image_thumb 	= wp_get_attachment_image_src($key_2, 'thumbnail'); ?>
																<div class="item">
																	<a href="#" onclick="jQuery('.gallery-carousel').trigger('to.owl.carousel', [<?php echo $thumbnail ?>,300]); return false;">
																		<img class="img-thumbnail" src="<?php echo $image_thumb[0] ?>">
																	</a>
																</div>
															<?php } ?>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="product-details">
												<h1 class="title">
													<span><?php the_title() ?></span>
												</h1>

												<?php
													$get_availability = get_post_meta(get_the_ID(), 'p_availability', true); 
													switch ($get_availability) {
														case 'In Stock':
															$availability = '<span class="in-stock">'.$get_availability.'</span>';
															break;
															
															case 'Out of Stock':
															$availability = '<span class="out-of-stock">'.$get_availability.'</span>';
															break;
														
														default:
															$availability = '<span class="in-stock">'.$get_availability.'</span>';
															break;
													}
												?>

												<div class="category-block">
													<label for="">CATEGORY:</label> 
													<?php $terms = wp_get_post_terms( get_the_ID(), array( 'product_category' ) ); ?>
													<?php foreach ( $terms as $term ) : ?>
														<a href="<?php echo get_term_link($term->term_id) ?>"><?php echo $term->name; ?></a>
													<?php endforeach; ?>
												</div>

												<div class="availability-block"><label for="">AVAILABILITY:</label> <?php echo $availability ?></div>

												<div class="price-block"><label for="">PRICE:</label> <span>Request Quote</span></div>
												
												<div class="quantity-block">
													<label for="">QUANTITY:</label> <input type="number" pattern="[0-9]*" class="form-control" id="quantity-field" name="quantity-field">
												</div>

												<div class="add-cart-block">
													<button class="btn btn-site" type="submit">ADD TO ENQUIRY CART</button>
												</div>

												<div class="social-media-block">
													<?php get_template_part( 'template-parts/share-buttons' ); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<?php
								$p_specification = get_post_meta(get_the_ID(), 'p_product_specification', true);
								$p_description = get_post_meta(get_the_ID(), 'p_product_description', true);
								$p_description_specification = get_post_meta(get_the_ID(), 'p_description_specification', true);
								$p_demonstration = get_post_meta(get_the_ID(), 'p_product_demonstration', true);
								$p_portfolio = get_post_meta(get_the_ID(), 'p_product_portfolio', true);
								$p_faq = get_post_meta(get_the_ID(), 'p_product_faq', true);
							?>

							<div class="product-specs">
								<?php if(trim($p_description_specification) != '') { ?>
									<div class="specs">
										<div class="specs-header">
											Description & Specification
										</div>
										<div class="specs-content">
											<?php echo do_shortcode(wpautop($p_description_specification)) ?>
										</div>
									</div>
								<?php } ?>

								<?php if($p_demonstration != '') { ?>
									<div class="specs">
										<div class="specs-header">
											Product Demonstration
										</div>
										<div class="specs-content">
											<?php echo do_shortcode(wpautop($p_demonstration)) ?>
										</div>
									</div>
								<?php } ?>

								<div class="specs portfolio">
									<div class="specs-header">
										Our Portfolio
									</div>
									<div class="specs-content">
										
									</div>
								</div>
								
								<?php if($p_portfolio != '') { ?>
									<div class="specs portfolio">
										<div class="specs-header">
											Our Portfolio
										</div>
										<div class="specs-content">
											<div class="portfolio-grid">
												<div class="row">
													<?php foreach ($p_portfolio as $key_id => $portfolio) { ?>
													<div class="col-lg-3 col-md-4 col-6">
														<?php $caption = wp_get_attachment_caption( $key_id ); ?>
														<a href="<?php echo $portfolio ?>" data-fancybox="portfolio" data-caption="<?php echo $caption ?>">
															<div class="portfolio-img">
																<div class="item" style="background-image:url('<?php echo $portfolio ?>')"></div>
																<?php if($caption != '') { ?>
																	<div class="portfolio-caption"><?php echo $caption ?></div>
																<?php } ?>
															</div>
														</a>
													</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
								
								<?php if(!empty($p_faq)) { ?>
									<div class="specs faq">
										<div class="specs-header">
											FAQ
										</div>
										<div class="specs-content">
											<?php echo wpautop($p_faq) ?>
										</div>
									</div>
								<?php } ?>
							</div> <!-- product-specs -->

						</form>
						
						<div class="product-navigation">
							<div class="row">
								<div class="col-6 col-md-6 col-lg-6">
									<?php $prev_post = get_previous_post(); ?>
									<?php $prev_title = strip_tags(str_replace('"', '', $prev_post->post_title)); ?>
									<?php 
										$product_thumbnail_id 		= get_post_thumbnail_id( $prev_post->ID );
										$product_thumbnail_array 	= wp_get_attachment_image_src($product_thumbnail_id, 'thumbnail');
										$product_thumbnail 			= ($product_thumbnail_array[0] != '') ? $product_thumbnail_array[0] : get_bloginfo('template_url').'/dist/images/no-image.jpg';
									?>
									<?php if($prev_post) { ?>
										<a rel="prev" href="<?php echo get_permalink($prev_post->ID) ?>" title="<?php echo $prev_title ?>" class="">
											<div class="navigate-posts previous">
												<div class="label">
													<i class="fas fa-long-arrow-alt-left"></i> Previous Product
												</div>
												<div class="nav-post">
													<div class="post-thumb">
														<img src="<?php echo $product_thumbnail ?>" alt="<?php echo $prev_title ?>" class="img-thumbnail">
													</div>
													<div class="post-title">
														<?php echo $prev_title ?>
													</div>
												</div>
											</div>
										</a>
									<?php } ?>
								</div>
								<div class="col-6 col-md-6 col-lg-6">
									<?php $next_post = get_next_post(); ?>
									<?php $next_title = strip_tags(str_replace('"', '', $next_post->post_title)); ?>
									<?php 
										$product_thumbnail_id 		= get_post_thumbnail_id( $next_post->ID );
										$product_thumbnail_array 	= wp_get_attachment_image_src($product_thumbnail_id, 'thumbnail');
										$product_thumbnail 			= ($product_thumbnail_array[0] != '') ? $product_thumbnail_array[0] : get_bloginfo('template_url').'/dist/images/no-image.jpg';
									?>
									<?php if($next_post) { ?>
										<a rel="next" href="<?php echo get_permalink($next_post->ID) ?>" title="<?php echo $next_title ?>" class=" ">
											<div class="navigate-posts next">
												<div class="label">
													Next Product <i class="fas fa-long-arrow-alt-right"></i>
												</div>
												<div class="nav-post">
													<div class="post-thumb">
														<img src="<?php echo $product_thumbnail ?>" alt="<?php echo $next_title ?>" class="img-thumbnail">
													</div>
													<div class="post-title">
														<?php echo $next_title ?>
													</div>
												</div>
											</div>
										</a>
									<?php } ?>
								</div>
							</div>
						</div>

					</div>

				</div> <!-- Main Content -->

			</div>

		</div>

	</div>

</div>

<?php get_footer(); ?>
