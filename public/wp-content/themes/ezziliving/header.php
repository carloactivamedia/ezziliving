<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo get_bloginfo('template_url') ?>/dist/images/favicon.png" type="image/x-icon" />
	
	<title><?php wp_title(''); ?></title>

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

	<?php wp_head(); ?>

	<style type="text/css">

		@font-face {
			font-family: 'Gotham Medium';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Medium.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Medium.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Medium.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Medium.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Medium.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Medium.svg#Gotham-Medium') format('svg');
			font-weight: 500;
			font-style: normal;
		}

		@font-face {
			font-family: 'Gotham Bold';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Bold.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Bold.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Bold.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Bold.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Bold.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/Gotham-Bold.svg#Gotham-Bold') format('svg');
			font-weight: bold;
			font-style: normal;
		}
		
		@font-face {
			font-family: 'Gotham Rounded Book';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Book.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Book.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Book.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Book.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Book.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Book.svg#GothamRounded-Book') format('svg');
			font-weight: normal;
			font-style: normal;
		}

		@font-face {
			font-family: 'Gotham Rounded Light';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Light.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Light.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Light.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Light.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Light.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Light.svg#GothamRounded-Light') format('svg');
			font-weight: 300;
			font-style: normal;
		}

		@font-face {
			font-family: 'Gotham Rounded Medium';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Medium.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Medium.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Medium.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Medium.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Medium.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/GothamRounded-Medium.svg#GothamRounded-Medium') format('svg');
			font-weight: 500;
			font-style: normal;
		}
		
		@font-face {
			font-family: 'Avenir LT Std';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/AvenirLTStd-Roman.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/AvenirLTStd-Roman.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/AvenirLTStd-Roman.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/AvenirLTStd-Roman.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/AvenirLTStd-Roman.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/AvenirLTStd-Roman.svg#AvenirLTStd-Roman') format('svg');
			font-weight: normal;
			font-style: normal;
		}

	</style>

</head>

<body <?php body_class(); ?>>

<!-- HEADER -->
<div id="header-wrapper">

	<div class="top-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="address">
					<i class="fas fa-map-marker-alt"></i> 63 Jalan Pemimpin #02-09 Pemimpin Industrial Building, Singapore 577219
					</div>
				</div>
				<div class="col-lg-6">
					<div class="opening-hour">
						Showroom Opening Hours: Monday to Friday - 10am to 6pm | Saturday - 10am to 4pm
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="sticky-header">
		<div class="middle-header">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-6">
						<div class="header-logo">
							<a href="<?php echo get_bloginfo('url') ?>">
								<img src="<?php echo get_bloginfo('template_url') ?>/dist/images/logo.png" alt="Ezzi Living">
							</a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-6">
						<div class="call-us-at-wrapper">
							<div class="call-us-at-icon">
								<img src="<?php echo get_bloginfo('template_url') ?>/dist/images/call-us-icon-v2.png" alt="">
							</div>
							<div class="call-us-at-text-wrapper">
								<div class="call-us-at-text">Call us at</div>
								<div class="call-us-at-number">
									<a href="tel:6259 9928">
										6259 9928
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="bottom-header">
			<div class="container">
			
				<?php 
					// if ( has_nav_menu( 'primary' ) ) :
					// 	$args = array(
					// 		'menu' 				=> 'primary',
					// 		'theme_location'  	=> 'primary',
					// 		'container_class' 	=> 'navbar-collapse collapse',
					// 		'container_id' 		=> 'navbarSupportedContent',
					// 		'menu_class'		=> 'navbar-nav',
					// 		'depth' 			=> 4,
					// 		'walker'			=> new wp_bootstrap_navwalker);
					// 	wp_nav_menu($args);
					// endif;
				?>

				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
						<li class="nav-item active">
							<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Link</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Dropdown
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">Something else here</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link disabled" href="#">Disabled</a>
						</li>
						</ul>
						<form class="form-inline my-2 my-lg-0">
						<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
						<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
						</form>
					</div>
				</nav>
				
			</div>
		</div>
	</div>
	
</div> <!-- header-wrapper -->