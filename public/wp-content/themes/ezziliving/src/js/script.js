jQuery(function($) {
	'use strict';

	var header = $("#header-wrapper .sticky-header").height();

	// fix_grid_height();

	// setTimeout(function(){
	// 	fix_grid_height();
	// }, 2000);

	// var Shuffle = window.Shuffle;
	// var element = document.querySelector('.my-shuffle-container');
	// var sizer = element.querySelector('.my-sizer-element');

	// var shuffleInstance = new Shuffle(element, {
	// itemSelector: '.picture-item',
	// 	sizer: sizer // could also be a selector: '.my-sizer-element'
	// });

	
	// $(".cart-container a").html('<i class="fas fa-shopping-cart"></i>');

	if($(".cart-container a").length > 0 || $("#cart-link").length > 0) {
		var counter = '';
		$(".cart-container a").html('<i class="fas fa-shopping-cart"></i>');
		$.ajax({
			type: 'post',
			url: wpajax.ajax_url,
			data: 'action=cart_counter',
			dataType: 'json',
			success: function(res){
				if(res.counter !== undefined) {
					counter = '<span class="cart-counter">'+res.counter+'</span>';
				}
				
				if($(".cart-container a").length > 0) {
					$(".cart-container a").html('<i class="fas fa-shopping-cart"></i>'+counter);
				}

				if($("#cart-link").length > 0) {
					$("#cart-link").html('<i class="fas fa-shopping-cart"></i>'+counter);
				}
			}
		});
	}

	if($(".form-container").length > 0) {
		$.ajax({
			type: 'post',
			url: wpajax.ajax_url,
			data: 'action=search_form&s='+get_url_parameter('s'),
			dataType: 'json',
			success: function(res){
				$(".form-container").html(res.html);
			}
		});
	}

	// // init Masonry
	// var $grid = $('.masonry').masonry({
	// 	stamp: '.stamp'
	// });
	// // // layout Masonry after each image loads
	// // $grid.imagesLoaded().progress( function() {
	// // 	$grid.masonry('layout');
	// // });
	

	if (jQuery(this).scrollTop() > 1) {
		$("#header-wrapper").addClass('fixed');
	}

	jQuery(window).scroll(function () {
		var w_width = $(window).width();
		if(w_width <= 425) {
			header = $("#header-wrapper .sticky-header").height();
		} else {
			// header = $("#header-wrapper").height();
		}

		if (jQuery(this).scrollTop() >= (header + 8)) {
			$("#content-wrapper").css('padding-top', (header + 8));
			$("#header-wrapper").addClass('fixed');
		} else {
			$("#content-wrapper").css('padding-top', 0);
			$("#header-wrapper").removeClass('fixed');
		}
	});

	$(document).on('click', '.update-cart', function(e) {
		e.preventDefault();

		var $this = this;
		var submit_action = $($this).data('action');
		var quantity = $($this).closest('tr').find('input[name="quantity"]').val();
		var product = $($this).data('product');
		
		console.log(submit_action);
		console.log(product);
		
		var data = {
			submit_action : submit_action,
			product : product,
			quantity : quantity,
			action : 'updateCartAction',
			order : get_url_parameter('order')
		}
		
		console.log(data);

		$.ajax({
			type: 'post',
			url: wpajax.ajax_url,
			data: data,
			dataType: 'json',
			beforeSend: function(){
				$("tr.row-"+product).find('button').attr('disabled', 'disabled');
				$('.form-notification').html('');
			},
			success: function(res){
				if(res.success) {
					if(res.action == 'remove-row') {
						$("tr.row-"+product).fadeOut().remove();
						

						if($(".cart-row").length < 1) {
							console.log('enquiry cart is empty');
							$("#cart-form").hide().remove();
							$('.form-notification').html('<div class="alert alert-warning" role="alert"><p><strong>Your Enquiry Cart is empty!</strong><br><small>redirecting to the product page shortly.</small></p></div>');
							setTimeout(function() {
								location.href = res.url;
							}, 5000);
						}

						if($(".cart-container a").length > 0 || $("#cart-link").length > 0) {
							var counter = '';
							$(".cart-container a").html('<i class="fas fa-shopping-cart"></i>');
							$.ajax({
								type: 'post',
								url: wpajax.ajax_url,
								data: 'action=cart_counter',
								dataType: 'json',
								success: function(res){
									if(res.counter !== undefined) {
										counter = '<span class="cart-counter">'+res.counter+'</span>';
									}
									
									if($(".cart-container a").length > 0) {
										$(".cart-container a").html('<i class="fas fa-shopping-cart"></i>'+counter);
									}
					
									if($("#cart-link").length > 0) {
										$("#cart-link").html('<i class="fas fa-shopping-cart"></i>'+counter);
									}
								}
							});
						}
					}
				}

			},
			complete: function(){
				console.log('complete');
				$("tr.row-"+product).find('button').each(function(){
					$(this).removeAttr('disabled');
				});
			}
		});
	});

	$('#to-top').on('click', function () {
		jQuery('html, body').animate({scrollTop: '0px'}, 800);
		return false;
	});

	$('.sidebyside-carousel').owlCarousel({
		items: 1,
	});

	$('#testimonials-carousel').owlCarousel({
		items: 1,
		lazyLoad: true,
		loop: true,
		autoHeight:true,
		dot: true
	})
	

	var gallery_carousel = $(".gallery-carousel");
	gallery_carousel.on('initialized.owl.carousel', function(event){ 
		$(".gallery-wrapper").css('visibility', 'visible');
	});

	gallery_carousel.owlCarousel({
		dots: false,
		autoHeight: true,
		items: 1,
		lazyLoad:true
	});
	
	$("#gallery-thumbnail").owlCarousel({
		dots: false,
		items: 4,
		margin: 10,
		stagePadding: 20
	});

	// var $grid = $('.grid').isotope({
	// 	// options...
	// 	itemSelector: '.element-item'
	// });

	// // filter items on button click
	// $('.filter-button-group').on( 'click', 'button', function() {
	// 	var filterValue = $(this).attr('data-filter');
	// 	$grid.isotope({ filter: filterValue });
	// });

	$.validate({
		form : 'form#cart-form',
		errorMessagePosition : 'top',
		scrollToTopOnError : true,
		validateOnBlur : false,
		beforeValidation: function() {
			$('.form-notification').html('');
		},
		onSuccess: function(form){
			var $form = form;
			console.log($form);
			$.ajax({
				type: 'post',
				url: wpajax.ajax_url,
				data: $($form).serialize()+'&action=checkoutCart&order='+get_url_parameter('order'),
				dataType: 'json',
				beforeSend: function(){
					$($form).find('input[type="submit"]').attr('disabled', 'disabled');
					$('.form-notification').html('');
				},
				success: function(res){
					console.log(res);
					if(res.success) {
						location.href = res.url;
					} else if (!res.success) {
						$('.form-notification').html('<div class="alert alert-danger" role="alert"><p>'+res.message+'</p></div>');
						$('html, body').animate({
							scrollTop: ($("#form-notification").offset().top)-50
						}, 500);
					} else {
						$('.form-notification').html('<div class="alert alert-danger" role="alert"><p>There an error occurred while processing your request. Please try again later</p></div>');
					}
				},
				complete: function(){
					$($form).find('input[type="submit"]').removeAttr('disabled');
				}
			});
			return false;
		}
	});

	/**
	 * Fix grid height
	 */
	function fix_grid_height() {
		if(jQuery('.fix-height').length > 0) {
			// Resize the size
			jQuery('.fix-height').height('auto');

			// Only higher than Tablet Viewport
			if(jQuery(window).width() >= 768) {
				if(jQuery('.fix-height:not([data-fix-height-group])').length > 0) {
					var d = new Date();
					var n = d.getTime();
					jQuery('.fix-height:not([data-fix-height-group])').each(function() {
						jQuery(this).attr('data-fix-height-group', n);
					});
				}

				var untrimmed_groups = jQuery('.fix-height').map(function() {
					return jQuery(this).attr('data-fix-height-group');
				}).get();

				var trimmed_groups = untrimmed_groups.filter( only_unique_values );

				for (var i = trimmed_groups.length - 1; i >= 0; i--) {

					var selector = jQuery('.fix-height[data-fix-height-group="'+trimmed_groups[i]+'"]');

					var elementHeights = selector.map(function() {
						return jQuery(this).height();
					}).get();

					// Math.max takes a variable number of arguments
					// `apply` is equivalent to passing each height as an argument
					var maxHeight = Math.max.apply(null, elementHeights);

					// Set each height to the max height
					selector.height(maxHeight);
				}
			}
		}
	}

	function only_unique_values(value, index, self) { 
		return self.indexOf(value) === index;
	}

	function get_url_parameter(name) {
		return decodeURI(
			(RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
	}

	// function asd(url) {
	// 	var jWindow = $(window).width();
	// 	console.log(jWindow);
	// 	console.logurl');
	// 	// if ( jWindow >= 992 ) {
	// 		$.fancybox({
	// 			'href' : url
	// 		});
			
	// 		return false;
	// 	// }
	// }
	
});