// /////////////////////////////////////////
// Required
// /////////////////////////////////////////
var gulp 			= require('gulp'),
	uglify 			= require('gulp-uglify'),
	browserSync 	= require('browser-sync').create(),
	sass 			= require('gulp-sass'),
	concat 			= require('gulp-concat'),
	plumber 		= require('gulp-plumber'),
	autoprefixer 	= require('gulp-autoprefixer'),
	del 			= require('del');


// /////////////////////////////////////////
// Configuration
// /////////////////////////////////////////

var wp_theme_name 		= 'ezziliving/';
var broser_sync_proxy 	= 'local.ezziliving';
var broser_sync_port 	= 3037;
var theme_location 		= 'public/wp-content/themes/'+wp_theme_name;

var config = {
	js_concat_files: [
		'node_modules/jquery/dist/jquery.min.js',
		theme_location+'/src/js/popper.min.js',	
		'node_modules/bootstrap/dist/js/bootstrap.min.js',
		'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
		'node_modules/jquery-form-validator/form-validator/jquery.form-validator.min.js',
		'node_modules/owl.carousel/dist/owl.carousel.min.js',
		'node_modules/imagesloaded/imagesloaded.pkgd.min.js',
		'node_modules/masonry-layout/dist/masonry.pkgd.min.js',
		theme_location+'/src/js/script.js'
	],
	css_concat_files: [
		theme_location+'src/scss/style-template.scss',
		'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
		'node_modules/owl.carousel/dist/assets/owl.theme.default.min.css',
		'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css',
		theme_location+'/src/scss/style.scss'
	],
	js_filename: 'script.min.js',
	css_filename: 'style.css',
	css_destination: theme_location,
	js_destination: theme_location+'/dist/js/'
}


// /////////////////////////////////////////
// Scripts Tasks
// /////////////////////////////////////////
gulp.task('scripts', function(){
	gulp.src(config.js_concat_files)
		.pipe(plumber())
		.pipe(uglify())
		.pipe(concat(config.js_filename))
		.pipe(gulp.dest(config.js_destination))
		.pipe(browserSync.stream());
});



// /////////////////////////////////////////
// Sass Tasks
// /////////////////////////////////////////
gulp.task('sass', function(){
	gulp.src(config.css_concat_files)
		.pipe(plumber())
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(autoprefixer({
			browsers: [
				"Android 2.3",
				"Android >= 4",
				"Chrome >= 20",
				"Firefox >= 24",
				"Explorer >= 8",
				"iOS >= 6",
				"Opera >= 12",
				"Safari >= 6"
			],
			cascade: false
		}))
		.pipe(concat(config.css_filename))
		.pipe(gulp.dest(config.css_destination))
		.pipe(browserSync.stream());
});


// /////////////////////////////////////////
// Build Tasks
// /////////////////////////////////////////

// clear out all files and folders from build folder
gulp.task('build:remove', function(){
	return del([
		'build/**'
	]);
});

// task to create build directory for all files
gulp.task('build:copy', ['build:remove'], function(){
	return gulp.src('app/**/*/')
	.pipe(gulp.dest('build/'));
});

// task to remove unwanted build files
// list all files and directories here that you don't want to include
gulp.task('build:clean', ['build:copy'], function(){
	del(config.build_files_folder_remove);
});

// build task sequence
gulp.task('build', ['build:remove', 'build:copy', 'build:clean']);


// /////////////////////////////////////////
// Browser-Sync Tasks
// /////////////////////////////////////////
gulp.task('browser-sync', function(){
	browserSync.init({
		host: broser_sync_proxy,
		proxy: broser_sync_proxy,
		port: broser_sync_port,
		open: false
	})
	// browserSync.init({
	// 	proxy: "soundplanning.dev"
	// });
});


// /////////////////////////////////////////
// Watch Tasks
// /////////////////////////////////////////
gulp.task('watch', function(){
	gulp.watch(theme_location+'/src/js/**/*.js', ['scripts']);
	gulp.watch(theme_location+'/src/scss/**/*.scss', ['sass']);
	gulp.watch(theme_location+'/src/css/**/*.css', ['sass']);
});


// /////////////////////////////////////////
// Default Tasks
// /////////////////////////////////////////
gulp.task('default', ['sass', 'scripts', 'browser-sync', 'watch']);